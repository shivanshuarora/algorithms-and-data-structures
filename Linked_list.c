#include<stdio.h>

struct List{
	int data;
	struct List *next;
};


int list_length(struct List *head){
	struct List *current = head;
	int count = 0;
	while(current != NULL) {
		count++;
		current = current->next;
	}
	return count;
}

void insert_Linked_List(struct List **head, int data, int position) {
	int k = 1;
	struct List *newnode, *p, *q;
	newnode = (struct List *)malloc(sizeof(struct List));
	if(!newnode) {
		printf("memory error");
		return ;
	}
	
	newnode->data = data;
	newnode->next = NULL;
	p = *head;
	
	if(position == 1) {   // insert at the beginning
		newnode->next = p;
		*head = newnode;
	}
	else {
		//traverse the list until we find the position where we want to insert
		while((p != NULL) && k<position) {
			k++;
			q = p;
			p = p->next;
		}
		q->next = newnode;
		newnode->next = p;
	}
}

void delete_node(struct List **head, int position) {
	int k =1;
	struct List *p, *q;
	if(*head == NULL) {
		printf("List empty");
	}
	p = *head;
	// delete the 1st node
	if(position == 1) {
		*head = (*head)->next;
		free(p);
	}
	else {
			//traverse the list until we find the position where we want to insert
		while((p != NULL) && k<position) {
			k++;
			q = p;
			p = p->next;	
		}
		if(p == NULL) {
			printf("No such position");
		}
		else {
			q->next = p->next;
			free(p);
		}
	}
}




void main() {
	char c;
	struct List *curr,*head;
	int data, position;
	head = NULL;
	while(1) {
	
		printf("Do you want to insert or delete i/d \n");
		scanf("%c",&c);
		if(c == 'i') {
			printf("enter the element and the position where you want to insert");
			scanf("%d%d",&data,&position);
			insert_Linked_List(&head,data,position);
		}
		else if(c == 'd') {
			printf("enter the position where you want to delete");
			scanf("%d",&position);
			delete_node(&head,position);
		}
		
		curr = head;
		while(curr) {
    		printf("%d ", curr->data);
      		curr = curr->next ;
		}
	}
}


